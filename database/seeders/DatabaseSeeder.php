<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Post;
use App\Models\Category;
use App\Models\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        
        // User::create([
        //     'name' => 'Muhamad Zainal',
        //     'email' => 'Zainal@gmail.com',
        //     'password' => bcrypt('12345'),
        // ]);

        // User::create([
        //     'name' => 'Jaenalll',
        //     'email' => 'Jaenall@gmail.com',
        //     'password' => bcrypt('12345'),
        // ]);
        
        \App\Models\User::factory(3)->create();

        Category::create([
            'name' => 'Web Development',
            'slug' => 'web-development',
        ]);

        Category::create([
            'name' => 'Web Personal',
            'slug' => 'web-personal',
        ]);

        Category::create([
            'name' => 'Web Design',
            'slug' => 'web-design',
        ]);

        Post::factory(20)->create();

        // Post::create([
        //     'title' => 'Judul Pertama',
        //     'slug' => 'judul-pertama',
        //     'excerpt' => 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Iusto totam alias cupiditate id, molestias expedita saepe rerum repellat beatae corrupti quos soluta officia est minima consectetur, dolores sunt?',
        //     'body' => 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Iusto totam alias cupiditate id, molestias expedita saepe rerum repellat beatae corrupti quos soluta officia est minima consectetur, dolores sunt? Aperiam voluptatum, aspernatur sint consectetur sapiente debitis reprehenderit perspiciatis quis accusantium? Laborum quis asperiores soluta aliquid fugit blanditiis porro. Officia, id. Illum suscipit provident corporis repellat inventore praesentium. Veniam et tenetur dolor fugiat nam expedita maiores molestiae suscipit optio recusandae velit amet neque magni voluptas atque placeat, sint consequatur quasi facere culpa eaque nisi libero? Suscipit laudantium voluptates alias perferendis rerum quibusdam ratione nulla velit eveniet dolore? Voluptatum distinctio deleniti illum voluptatem.',
        //     'category_id' => 1,
        //     'user_id' => 1,
        // ]);
        // Post::create([
        //     'title' => 'Judul Kedua',
        //     'slug' => 'judul-kedua',
        //     'excerpt' => 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Iusto totam alias cupiditate id, molestias expedita saepe rerum repellat beatae corrupti quos soluta officia est minima consectetur, dolores sunt?',
        //     'body' => 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Iusto totam alias cupiditate id, molestias expedita saepe rerum repellat beatae corrupti quos soluta officia est minima consectetur, dolores sunt? Aperiam voluptatum, aspernatur sint consectetur sapiente debitis reprehenderit perspiciatis quis accusantium? Laborum quis asperiores soluta aliquid fugit blanditiis porro. Officia, id. Illum suscipit provident corporis repellat inventore praesentium. Veniam et tenetur dolor fugiat nam expedita maiores molestiae suscipit optio recusandae velit amet neque magni voluptas atque placeat, sint consequatur quasi facere culpa eaque nisi libero? Suscipit laudantium voluptates alias perferendis rerum quibusdam ratione nulla velit eveniet dolore? Voluptatum distinctio deleniti illum voluptatem.',
        //     'category_id' => 1,
        //     'user_id' => 1,
        // ]);
        // Post::create([
        //     'title' => 'Judul Ketiga',
        //     'slug' => 'judul-ketiga',
        //     'excerpt' => 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Iusto totam alias cupiditate id, molestias expedita saepe rerum repellat beatae corrupti quos soluta officia est minima consectetur, dolores sunt?',
        //     'body' => 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Iusto totam alias cupiditate id, molestias expedita saepe rerum repellat beatae corrupti quos soluta officia est minima consectetur, dolores sunt? Aperiam voluptatum, aspernatur sint consectetur sapiente debitis reprehenderit perspiciatis quis accusantium? Laborum quis asperiores soluta aliquid fugit blanditiis porro. Officia, id. Illum suscipit provident corporis repellat inventore praesentium. Veniam et tenetur dolor fugiat nam expedita maiores molestiae suscipit optio recusandae velit amet neque magni voluptas atque placeat, sint consequatur quasi facere culpa eaque nisi libero? Suscipit laudantium voluptates alias perferendis rerum quibusdam ratione nulla velit eveniet dolore? Voluptatum distinctio deleniti illum voluptatem.',
        //     'category_id' => 2,
        //     'user_id' => 1,
        // ]);
        // Post::create([
        //     'title' => 'Judul Keempat',
        //     'slug' => 'judul-keempat',
        //     'excerpt' => 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Iusto totam alias cupiditate id, molestias expedita saepe rerum repellat beatae corrupti quos soluta officia est minima consectetur, dolores sunt?',
        //     'body' => 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Iusto totam alias cupiditate id, molestias expedita saepe rerum repellat beatae corrupti quos soluta officia est minima consectetur, dolores sunt? Aperiam voluptatum, aspernatur sint consectetur sapiente debitis reprehenderit perspiciatis quis accusantium? Laborum quis asperiores soluta aliquid fugit blanditiis porro. Officia, id. Illum suscipit provident corporis repellat inventore praesentium. Veniam et tenetur dolor fugiat nam expedita maiores molestiae suscipit optio recusandae velit amet neque magni voluptas atque placeat, sint consequatur quasi facere culpa eaque nisi libero? Suscipit laudantium voluptates alias perferendis rerum quibusdam ratione nulla velit eveniet dolore? Voluptatum distinctio deleniti illum voluptatem.',
        //     'category_id' => 2,
        //     'user_id' => 2,
        // ]);
    }
}
