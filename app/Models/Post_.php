<?php

namespace App\Models;


class Post
{
    private static $blog_posts =[
        [
            "title" => "Judul Post Pertama",
            "author" => "Muhamad Zainal Muttaqin",
            "slug" => "judul-post-pertama",
            "body" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Quidem esse laudantium expedita blanditiis, cumque repellendus, autem dolor vero veritatis dicta accusamus rerum. Dolore omnis iusto vel quis incidunt, ratione dignissimos corrupti accusantium veritatis quia ex a sit est culpa sequi eum impedit! Porro animi nulla id tempore iure! Magni fugit voluptatibus eos explicabo possimus temporibus sint aperiam quaerat quam, amet similique voluptatem perspiciatis nihil dolorem voluptas architecto inventore quas iste maiores cumque praesentium nemo, veniam recusandae illo? At, neque dolorem.",
        ],
        [
            "title" => "Judul Post Kedua",
            "slug" => "judul-post-kedua",
            "author" => "Muhamad hehe",
            "body" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Corporis quibusdam, omnis temporibus unde, quidem culpa, ex quaerat possimus impedit commodi nemo totam voluptas consequuntur cum cumque molestias dolore tempore! Odit omnis natus earum odio consequuntur blanditiis, accusamus repudiandae, rerum eligendi quos placeat eos qui quisquam dignissimos similique expedita veritatis recusandae a facere. Magni, enim nesciunt hic deserunt ex fuga, asperiores corrupti laudantium at cupiditate quidem corporis amet rem architecto dolorum voluptates incidunt maxime? Accusantium soluta iste maiores tempora veritatis quo error sit quibusdam cupiditate molestiae ab perferendis hic facere voluptatibus at quos corporis consectetur, harum sunt autem corrupti blanditiis! Id.",
        ],
    ];

    public static function all()
    {
        return collect(self::$blog_posts);
    }

    public static function find($slug)
    {
        $posts = static::all();

        return $posts->firstWhere("slug", $slug);

    }
}
